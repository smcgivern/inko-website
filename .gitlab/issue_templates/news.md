<!-- Briefly describe the announcement. -->

## Before publishing

* [ ] Check for common spelling errors (`setlocal spell spelllang=en` in Vim)
* [ ] Make sure all links (if any) are working
* [ ] Set up MR: LINK
* [ ] Preview locally to make sure the article is rendering properly

## After publishing

* [ ] Submit to <https://www.reddit.com/r/inko/>: LINK
* [ ] Submit to <https://discourse.inko-lang.org/c/announcements>: LINK
* [ ] Tweet about it: LINK
* [ ] Post a link to the article in the Matrix channel
