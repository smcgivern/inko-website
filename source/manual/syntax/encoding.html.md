---
title: Encoding
---

All Inko source files must be UTF-8 encoded, even though Inko's own syntax only
uses characters in the ASCII range.
