---
title: Syntax
---

Inko's syntax is fairly straightforward. This section describes the various
aspects of the syntax. This section is meant for developers to better understand
the Inko syntax.

Various sections will include syntax definitions using
[EBNF](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form).

Despite this, this section is _not_ an official grammar and might be slightly
out of date from time to time.
