---
title: Manual Introduction
---

The Inko manual covers many aspects of Inko, such as: the syntax, how to use the
compiler, error handling, and much more. The manual is suitable for both
beginners and seasoned veterans.

The manual does not include documentation of the standard library, this will be
covered separately once a code documentation program has been developed.

You can contribute to the manual by submitting a merge request to the [Inko
website repository](https://gitlab.com/inko-lang/website).
