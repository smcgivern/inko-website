---
title: Community
keywords:
  - community
  - inko
  - gitlab
  - participate
description: Learn more about how to participate in the Inko community.
---

A healthy community is very important for any programming language, and Inko is
no exception. We are committed to providing a friendly, safe and welcoming
environment for all, regardless of level of experience, gender identity and
expression, sexual orientation, disability, personal appearance, body size,
race, ethnicity, age, religion, nationality, or other similar characteristic.
For more information, see the [code of conduct](/code-of-conduct).

## Services

The following services are currently provided:

* The official [forum powered by Discourse](https://discourse.inko-lang.org/).
* The official [sub-Reddit](https://www.reddit.com/r/inko/).
* A [Matrix community](https://matrix.to/#/+inko:matrix.org) for chatting with
  fellow members of the Inko community.

Everybody participating in the community is subject to the [code of
conduct](/code-of-conduct).

## Using Matrix

Using [Matrix](https://matrix.org) and [Riot.im](https://riot.im) can be a
little confusing. To make this easier, you can take the following steps to get
connected:

1. Open the [Inko channel](https://riot.im/app/#/room/#inko-lang:matrix.org)
   using Riot.
1. Click the link shown to join the discussion. This requires you to sign in to
   Riot/Matrix first.

If you simply want to view the discussion, without joining, you can use
[view.matrix.org](https://view.matrix.org/room/!CENmKDnMngVwJJrTry:matrix.org/).
