---
title: Documentation
keywords:
  - inko
  - documentation
  - getting started
description: |
  Documentation and other resources to help you get started with Inko.
---

For both new and experiences users, [the manual](/manual) will be very helpful.
This manual discusses a wide range of topics such as:

* The syntax
* Tutorials for new users
* Common patterns, such as error handling
* Compiler internals, such as type inference
* Virtual machine internals, such as the garbage collector

We also provide an [FAQ](/faq), where we collect frequently asked questions.
Standard library documentation is unfortunately not yet available, as we have
yet to write the necessary software to produce such documentation.
